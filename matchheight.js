import { arrayFrom, objectAssign, elementMatches } from './polyfills'

export class MatchHeight {
    constructor(selector) {
        this.elements = selector

        MatchHeight.prototype._previousResizeWidth = -1
        MatchHeight.prototype._updateTimeout = -1
        MatchHeight.prototype._groups = []
        MatchHeight.prototype._throttle = 80
        MatchHeight.prototype._maintainScroll = false
        MatchHeight.prototype._beforeUpdate = null
        MatchHeight.prototype._afterUpdate = null

        window.onload = function (event) {
            MatchHeight.prototype.resize(false, event)
        }

        window.onresize = function (event) {
            MatchHeight.prototype.resize(false, event)
        }

        document.addEventListener('orientationchange', function (event) {
            MatchHeight.prototype.resize(true, event)
        })
    }

    static _parse(value) {
        // Parse value and convert NaN to 0
        return parseFloat(value) || 0
    }

    static _rows(elements) {
        const tolerance = 1
        let lastTop = null
        let rows = []

        Array.from(elements).forEach(element => {
            const top = element.getBoundingClientRect().top - MatchHeight._parse(element.style.marginTop)

            if (rows.length === 0) {
                // First item on the row, so just push it
                rows.push([element])
            } else {
                // If the row top is the same, add to the row group
                if (Math.floor(Math.abs(lastTop - top)) <= tolerance) {
                    rows[rows.length - 1].push(element)
                } else {
                    // otherwise start a new row group
                    rows.push([element])
                }
            }

            // Keep track of the last row top
            lastTop = top
        })

        return rows
    }

    static _parseOptions(options) {
        let opts = {
            byRow: true,
            property: 'height',
            target: null,
        }

        if (typeof options === 'object')
            return Object.assign(opts, options)

        if (typeof options === 'boolean') {
            opts.byRow = options
        }

        return opts
    }

    init(options) {
        arrayFrom()
        objectAssign()
        elementMatches()
        
        const opts = MatchHeight._parseOptions(options)

        if (this.elements.length <= 1 && !opts.target) {
            return this
        }

        // keep track of this group so we can re-apply later on load and resize events
        this._groups.push({
            elements: this.elements,
            options: opts,
        })

        // match each element's height to the tallest element in the selection
        MatchHeight._apply(this.elements, opts)

        return this
    }

    static _apply(elements, options) {
        const opts = MatchHeight._parseOptions(options)
        let rows = [elements]

        // get rows if using byRow, otherwise assume one row
        if (opts.byRow && !opts.target) {
            // must first force an arbitrary equal height so floating elements break evenly
            Array.from(elements).forEach(element => {
                let display = element.style.display

                // temporarily force a usable display value
                if (display !== 'inline-block' && display !== 'flex' && display !== 'inline-flex') {
                    display = 'block'
                }

                if (element.getAttribute('style') !== null)
                    element.setAttribute('style-cache', element.getAttribute('style'))

                element.style.display = `'${display}'`
                element.style.paddingTop = '0'
                element.style.paddingBottom = '0'
                element.style.marginTop = '0'
                element.style.marginBottom = '0'
                element.style.borderTopWidth = '0'
                element.style.borderBottomWidth = '0'
                element.style.height = '100px'
                element.style.overflow = 'hidden'
            })

            // get the array of rows (based on element top position)
            rows = MatchHeight._rows(elements)

            Array.from(elements).forEach(element => {
                element.setAttribute('style', element.getAttribute('style-cache') || '')
            })
        }

        Array.from(rows).forEach(row => {
            let targetHeight = 0

            if (!opts.target) {
                // skip apply to rows with only one item
                if (opts.byRow && row.length <= 1) {
                    row[0].style.height = ''
                    return
                }

                // iterate the row and find the max height
                row.forEach(el => {
                    let style = el.getAttribute('style')
                    let display = el.style.display

                    // temporarily force a usable display value
                    if (display !== 'inline-block' && display !== 'flex' && display !== 'inline-flex') {
                        display = 'block'
                    }

                    // ensure we get the correct actual height (and not a previously set height value)
                    el.style.display = `'${display}`
                    el.style.height = ''

                    // find the max height
                    if (el.offsetHeight > targetHeight) {
                        targetHeight = el.offsetHeight
                    }

                    // revert styles
                    if (style) {
                        el.setAttribute('style', style)
                    } else {
                        el.style.display = ''
                    }
                })
            } else {
                // if target set, use the height of the target element
                targetHeight = opts.target.offsetHeight
            }

            row.forEach(el => {
                let verticalPadding = 0

                // don't apply to a target
                if (opts.target && el.matches(opts.target))
                    return

                // handle padding and border correctly (required when not using border-box)
                if (el.style.boxSizing !== 'border-box') {
                    verticalPadding += MatchHeight._parse(el.style.borderTopWidth) + MatchHeight._parse(el.style.borderBottomWidth)
                    verticalPadding += MatchHeight._parse(el.style.paddingTop) + MatchHeight._parse(el.style.paddingBottom)
                }

                // set the height (accounting for padding and border)
                el.style.height = (targetHeight - verticalPadding) + 'px'
            })
        })

        return this
    }

    _update(event) {
        if (this._beforeUpdate) {
            this._beforeUpdate(event, this._groups)
        }

        Array.from(this._groups).forEach(el => {
            MatchHeight._apply(el.elements, el.options)
        })

        if (this._afterUpdate) {
            this._afterUpdate(event, this._groups)
        }
    }

    resize(throttle, event) {
        // prevent update if fired from a resize event
        // where the viewport width hasn't actually changed
        // fixes an event looping bug in IE8
        if (event && event.type === 'resize') {
            let windowWidth = window.innerWidth
            if (windowWidth === this._previousResizeWidth) {
                return
            }
            this._previousResizeWidth = windowWidth
        }

        // throttle updates
        if (!throttle) {
            MatchHeight.prototype._update(event)
        } else if (this._updateTimeout === -1) {
            this._updateTimeout = setTimeout(function () {
                MatchHeight.prototype._update(event)
                this._updateTimeout = -1
            }, this._throttle)
        }
    }
}